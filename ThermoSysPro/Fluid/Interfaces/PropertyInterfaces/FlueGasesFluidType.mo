within ThermoSysPro.Fluid.Interfaces.PropertyInterfaces;
type FlueGasesFluidType = enumeration(
    FlueGases "3 - Flue gases (compressible)") annotation (Documentation(info="<html>
<p><b>Copyright &copy; EDF 2002 - 2024</b> </p>
<p><b>ThermoSysPro Version 4.1</b> </p>
</html>"));
